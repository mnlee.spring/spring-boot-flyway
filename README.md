<a href="https://mnlee.gitlab.io/portfolio/#gitlab">Back to Margaret Lee Portfolio</a>


## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Execution](#execution)
* [Output](#output)

## General info
Flyway is a tool that implements incremental changes to database so that migration to a version is easily done.
	
## Technologies
Project is created with:
* Spring Boot version: 2.1.1
* Java version: 1.8
* MySql 5.7
* Maven 3.3.9
	
## Setup
* Install MySql 
* Create project using https://start.spring.io/ or Spring Boot CLI with dependencies-- web,mysql,data-jpa,flyway.


### Steps
* Create application.properties see resources/application.properties
* Create Domain Entity see User.java
* Create first Flyway migration script see resources/db/migration/v1.0__init.sql
	- Set the spring.jpa.hibernate.ddl-auto = create in the application.properties
	- Run the project
	- Check [Output](#output)
* Create second Flyway migration script see resources/db/migration/v1.1__add-data.sql
	- set the spring.jpa.hibernate.ddl-auto = validate in the application.properties
	- Run the project
	- Check [Output](#output)

## Execution
* mvn spring-boot:run
* create jar file
	- mvn clean install
	- java -jar <JARFILE>

## Output
* Check console for log pertaining to Flyway and table creation/update
* Check flyway_schema_history 
* Check database for output result



 